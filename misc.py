# - *- coding: utf- 8 - *-

###########
# misc.py #
###########


def make_values_pretty_int(values):
	"""
		Converts str values to int if it possible
	"""
	for i, v in enumerate(values):
		try:
			values[i] = int(v)
		except ValueError:
			values[i] = v if v != "" else None

	return values

def cast_safely(a, t):
	try:
		result = t(a)
	except:
		result = None
	return result

def cast_safely_array(a, t):
	result = []
	for v in a:
		result.append(cast_safely(v, t))
		
	return result

def get_available_comparators():
	return ["<", "<=", "=", ">=", ">"]