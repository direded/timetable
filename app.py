# - *- coding: utf- 8 - *-

from flask import Flask
from flask import request
from flask import render_template
from flask import g
from flask import jsonify

import model as M
import fields as F
import sqlbuilder as SQL
import misc as MISC

app = Flask(__name__)

@app.teardown_appcontext
def close_db(error):
	if hasattr(g, "fb_db"):
		g.fb_db.close()

@app.route("/")
def p_index():
	data = {}
	data["hrefs"] = [
		("p_tables", "Таблицы"),
		("p_schedule", "Расписание"),
		("p_tables_query", "Запросы к таблицам"),
		("p_tables_editor_get", "Изменение таблицы"),
	]
	return render_template("index.html", _title = "Демонстрация", **data)

@app.route("/tables")
def p_tables():
	model = M.get_model()
	data = {}
	selected = request.args.get("t", "")

	tables_caps = model.get_tables_caption()
	data["tables"] = tables_caps # now tables for html is not really tables lol

	if (selected.isdigit()):
		selected = int(selected)
		if (selected >= 0 and selected < len(tables_caps)):
			data["selected"] = selected
			data["table_columns"] = model.get_table(selected).get_fields_caption()
			sel = SQL.Select(table=model.get_table(selected))
			data["entries"] = SQL.execute(sel)
	return render_template("tables.html", _title = "Таблицы", **data)

@app.route("/schedule")
def p_schedule():
	model = M.get_model()
	data = {}

	s_field = request.args.get("f", "")
	s_value = request.args.get("v", "")

	sql = SQL.Select(table=model.get_table_by_class_name("T_SCHED_ITEMS").pretty)

	fields_caption = sql.get_fields_caption()
	data["table_fields"] = fields_caption

	if s_field.isdigit():
		s_field = int(s_field)
		if 0 <= s_field < len(fields_caption) and s_value != "":
			cond = SQL.M_COMP(sql.get_fields()[s_field], "LIKE", s_value)
			if cond.is_correct() == True:
				sql.where(cond)
			else: 
				s_value = "ERROR"


	data["s_field"] = s_field
	data["s_value"] = s_value
	data["entries"] = SQL.execute(sql)

	return render_template("schedule.html", _title = "Расписание", **data)

@app.route("/tables_query/<int:s_table>", methods=['GET', 'POST'] )
@app.route("/tables_query", methods=['GET', 'POST'])
def p_tables_query(s_table=None):
	model = M.get_model().pretty
	data = {}

	available_comps = ("<", "<=", "=", "<>", ">=", ">", "LIKE")

	if not s_table is None:

		table = model.get_table(s_table)
		
		sel = SQL.Select(table=table)
		
		if request.method == 'POST':

			fields = request.form.getlist("fields[]")
			comps = request.form.getlist("comps[]")
			values = request.form.getlist("values[]")

			page_num = request.form.get("page_num", int)			
			page_size = request.form.get("page_size", int)
			page_num = MISC.cast_safely(page_num, int)
			page_size = MISC.cast_safely(page_size, int)

			order_by = request.form.getlist("order_by[]")

			for i, v in enumerate(order_by):
				try:
					order_by[i] = int(order_by[i])
				except Exception:
					return "-1" 

				sel.order_by(order_by[i])

			if not page_size is None:
				sel.page_size = page_size
				sel.page_num = page_num if (not page_num is None) and page_num != -1  else 0

			if len(fields) != len(comps) or len(comps) != len(values):
				return -1

			MISC.make_values_pretty_int(values)

			cond = None

			for i, v in enumerate(fields):
				try:
					fields[i] = int(fields[i])
					comps[i] = int(comps[i])
				except TypeError:
					return "-1"
				
				temp = SQL.M_COMP(sel.get_fields()[fields[i]], available_comps[comps[i]], values[i])
				if temp.is_correct() != True:
					return "-1"
				
				if cond is None:
					cond = temp
				else:
					cond = SQL.M_COND(temp, cond, cond="AND")
					
			if not cond is None:
				sel.where(cond)

			data["count"] = sel.get_count(SQL.execute)
			data["entries"] = SQL.execute(sel.finish())
			return jsonify(data)

		data["count"] = sel.get_count(SQL.execute)
		data["table_fields"] = table.get_fields_caption()
		data["entries"] = SQL.execute(sel.finish())

	data["s_table"] = s_table
	data["comps"] = available_comps
	data["tables_caption"] = model.get_tables_caption()
	data["page_num"] = 0

	return render_template("tables_query.html", _title = "Запросы к таблицам", **data)

@app.route("/tables_editor/<int:s_table>", methods=['GET'] )
@app.route("/tables_editor", methods=['GET'])
def p_tables_editor_get(s_table=None):
	model = M.get_model().pretty
	data = {}
	data["s_table"] = s_table
	data["tables_caption"] = model.get_tables_caption()

	return render_template("tables_editor.html", _title = "Просмотр таблиц", **data)

@app.route("/tables_editor/<int:s_table>", methods=['POST'])
@app.route("/tables_editor", methods=['POST'])
def p_tables_editor_post(s_table=None):
	if s_table == None:
		return "error"
	act = request.form.get("action")
	if act is None:
		return "error"
	post = {}
	model = M.get_model().pretty
	table = model[s_table]

	if act == "get_entries":
		sel = SQL.Select(table=table)
		post["fields_caption"] = sel.get_fields_caption()
		post["entries"] = SQL.execute(sel.finish())
		return jsonify(post)
	return "error"

@app.route("/tables_editor/<int:s_table>/<int:s_entry>", methods=['GET'])
def p_tables_editor_entry_get(s_table=None, s_entry=None):
	if s_table is None or s_entry is None:
		return "wut?"
	model = M.get_model()

	data = {}
	data["s_table"] = s_table
	data["s_entry"] = s_entry
	table = model[s_table]
	data["legenda"] = "Таблица '{}'. Изменение записи #{}".format(table.caption, s_entry + 1)

	title = "Запись '{}' #{}".format(table.caption, str(s_entry + 1))
	return render_template("tables_editor_entry.html", _title=title, **data)

def compose_entry_form_data(table, values=None):
	if values is None:
		sel = SQL.Select(table=table)
		sel.page_size = 1
		values = SQL.execute(sel.finish())[0]

	fields = []
	i = -1
	for k, table_field in table.get_fields().items():
		i += 1
		if k == "id":
			continue
		field = {}
		field["caption"] = table_field.target.caption
		field["type"] = table_field.short() # can be "str", "int" or "foriegn"
		field["value"] = values[i]
		field["autocomplete_data"] = F.E_FIELD(table, k).get_values()
		fields.append(field)
	return fields


@app.route("/tables_editor/<int:s_table>/<int:s_entry>", methods=['POST'])
def p_tables_editor_entry_post(s_table=None, s_entry=None):
	if s_table is None or s_entry is None:
		return "wut?"

	model = M.get_model()
	table = model[s_table]

	sel = SQL.Select(table=table)
	sel.where(SQL.M_COMP(F.E_FIELD(table, 0), "=", s_entry + 1))
	values = SQL.execute(sel.finish())

	if len(values) == 0:
		return "no entry"
	values = values[0]

	act = request.form.get("action")
	if act is None:
		return "error"
	if act == "get":
		post = {}

		post["fields"] = compose_entry_form_data(table, values)
		return jsonify(post)
	if act == "update" or act == "delete":
		post = {}
		id = values[0] #
		values = values[1:]
		values = [str(v) for v in values]
		entered_data = request.form.getlist("entered_data[]")
		initial_data = request.form.getlist("initial_data[]")
		post["values"] = values
		for i, v in enumerate(initial_data):
			if v != values[i] and (entered_data[0] is None or entered_data[i] != values[i]):
				post["state"] = "confirm"
				break
		else:
			post["state"] = "success"
			up = None
			if act == "delete":
				up = SQL.Delete(table, id)
			else:
				up = SQL.Update(table, id)
				for i, v in enumerate(entered_data):
					up.set(i+1, v)
				post["values"] = entered_data
			try:
				SQL.execute(up.finish(), fetch=False, do_commit=True)
			except:
				return "error"

		if act == "update" or post["state"] == "confirm":
			sel = SQL.Select(table=table.pretty)
			sel.where(SQL.M_COMP(F.E_FIELD(table.pretty, 0), "=", s_entry + 1))
			post["value_captions"] = SQL.execute(sel.finish())[0][1:]
		return jsonify(post)
	return "error"

@app.route("/tables_editor/<int:s_table>/new", methods=['GET'])
def p_tables_editor_new_entry_get(s_table=None):
	if s_table is None:
		return "wut?"
	model = M.get_model()

	data = {}
	data["s_table"] = s_table
	table = model[s_table]
	data["legenda"] = "Таблица '{}'. Новая запись".format(table.caption)

	title = "Новая запись '{}'".format(table.caption)
	return render_template("tables_editor_entry_new.html", _title=title, **data)

@app.route("/tables_editor/<int:s_table>/new", methods=['POST'])
def p_tables_editor_new_entry_post(s_table=None):
	model = M.get_model()
	table = model[s_table]
	post = {}
	act = request.form.get("action")
	if act is None:
		return "error"
	if act == "get":
		post["fields"] = compose_entry_form_data(table)
	elif act == "create":
		entered_data = request.form.getlist("entered_data[]")
		print(entered_data)
		insert = SQL.Insert(table, entered_data)
		# SQL.execute(insert, fetch=False, do_commit=True)
		try:
			SQL.execute(insert, fetch=False, do_commit=True)
		except:
			return "error"

	return jsonify(post)

@app.route("/table_analytic", methods=['GET'])
def p_tables_analytic():
	model = M.get_model()
	data = {}
	data["tables_caption"] = model.get_tables_caption()
	return render_template("tables_analytic.html", _title="Аналитика таблиц", **data)


@app.route("/request", methods=['POST'])
def p_request():
	model = M.get_model()
	act = request.form.get("act")

	if act == "get_table_fields":
		table_id = request.form.get("table_id", type=int)
		if table_id is not None:
			return jsonify(model[table_id].pretty.get_fields_caption())
		else:
			return "error"

	if act == "get_available_comparators":
		return jsonify(MISC.get_available_comparators())
	
	if act == "get_table":
		table_id = request.form.get("table_id", type=int)
		if table_id is None:
			return "error"
		
		select = SQL.Select(table=model[table_id].pretty)

		cond_fields = request.form.getlist("cond_fields[]")
		cond_comps = request.form.getlist("cond_comps[]")
		cond_values = request.form.getlist("cond_values[]")

		if cond_fields is not None and len(cond_fields) > 0:
			conds = []
			available_comps = MISC.get_available_comparators()
			cond_fields = MISC.cast_safely_array(cond_fields, int)
			cond_comps = MISC.cast_safely_array(cond_comps, int)
			cond_values = MISC.make_values_pretty_int(cond_values)
			for i, f in enumerate(cond_fields):
				if cond_fields[i] is None or cond_comps[i] is None or cond_values[i] is None:
					return "error"
				conds.append(SQL.Compare(select.get_fields()[cond_fields[i]],
					available_comps[cond_comps[i]],
					cond_values[i]))
			condition = None
			if len(conds) < 2:
				condition = conds[0]
			else:
				condition = SQL.Condition(*conds, cond="AND")
			select.where(condition)  

		return jsonify(SQL.execute(select.finish()))

	if act == "get_fields_pretty_values": 
		table_id = request.form.get("table_id", type=int)
		if table_id is None:
			return "error"

		fields_id = request.form.getlist("fields_id[]")
		fields_id = MISC.make_values_pretty_int(fields_id)
		table = model[table_id]

		data = []
		for id in fields_id:
			data.append(F.E_FIELD(table, id).get_values())

		return jsonify(data)

		# cons

	return "error"

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html')