# - *- coding: utf- 8 - *-

#################
# sqlbuilder.py #
#################

import fdb
from flask import g

import fields as F
import model

import copy
import os

def get_db():
	if not hasattr(g, "fb_db"):
		g.fb_db = fdb.connect(
			# dsn = os.environ["FDB_PATH"],
			dsn = "/db/timetable/timetable.fdb",
			user = 'sysdba',
			password = 'masterkey',
			connection_class = fdb.ConnectionWithSchema,
			charset = 'UTF8',
		)

	return g.fb_db

def execute(smth, fetch=True, do_commit=False):
	vars = []
	con = get_db()
	cur = con.cursor()
	q = None
	if isinstance(smth, str):
		q = smth
	elif isinstance(smth, Base) or isinstance(smth, dict):
		f = smth
		if isinstance(smth, Base):
			f = smth.finish()
		q = f["query"]
		vars = f["vars"]
	else:
		raise(Exception("Bad var type: {} instead of str".format(smth.__class__)))
	q += ";"
	print(q, vars)
	try:
		cur.execute(q, vars)
	except:
		raise(Exception("Bas sql request"))
		return None

	if do_commit:
		con.commit()
	if fetch:
		return cur.fetchall()

def sql_value(a):
	res = ""
	if isinstance(a, F.E_FIELD):
		res = "{}.{}".format(a.table.name, a.field.name)
	else:
		res = "?"
	return res

def add_statement_with_vars(query, vars, statement, params):
	query += statement.format(params["query"])
	vars += params["vars"]
	return query, vars

class Base:
	pass

class Compare:
	# a and b are e_fields
	def __init__(self, a, cond: str, b): # TODO! Turn cond in class instance?
		self._a = a
		self._b = b
		self._cond = cond

		self._correct = self._is_correct()

	def __call__(self):
		q = "{} {} {}".format(sql_value(self._a), self._cond, sql_value(self._b))
		vars = []
		if sql_value(self._a) == "?":
			vars.append(self._a)
		if sql_value(self._b) == "?":
			vars.append(self._b)
		return {"query": q, "vars": vars}

	@property
	def a(self):
		return self._a

	@property
	def b(self):
		return self._b

	def _is_correct(self):
		a = self._a
		b = self._b
		if isinstance(a, F.E_FIELD):
			a = self._a.field

		if isinstance(b, F.E_FIELD):
			b = self._b.field

		if type(a) in F.primitives and type(b) in primitives:
			return None

		if type(a) in F.primitives:
			a = b.cast(a)
			if a is None:
				return False
			else:
				self._a = a

		elif type(b) in F.primitives:
			b = a.cast(b)
			if b is None:
				return False
			else:
				self._b = b

		else:
			return a.comparable(b)
		return True

	def is_correct(self):
		return self._correct

class Condition:
	def __init__(self, *args, cond):
		"""
			cond can be either "OR" or "AND"
			args is instances of Compare class
		"""
		self._cond = cond
		self._args = args

		if not (cond == "OR" or cond == "AND"):
			raise Exception("Wrong condition")

		if not isinstance(args, tuple) or isinstance(args, tuple) and len(args) < 2:
			raise Exception("Not enough args")

	def add_arg(self, a):
		self._args.append(a)

		return res

	def __call__(self):
		vars = []
		first = self._args[0]()
		res = "{}".format(first["query"])
		vars += first["vars"]
		for a in self._args[1:]:
			cmp = a()
			vars += cmp["vars"]
			res += " {} {}".format(self._cond, cmp["query"])
		return {"query": res, "vars": vars}

M_COMP = Compare
M_COND = Condition

class Join:
	def __init__(self, table, mod, cond):
		self._table = table
		self._mod = mod
		self._cond = cond

	def __call__(self):
		cond = self._cond()
		table = self._table.get_sql()
		query = "\n{}JOIN {} ON {}".format((self._mod.upper() + " ") if hasattr(self, "_mod") and not self._mod is None else "", table["query"], cond["query"])  
		return {"query": query, "vars": cond["vars"]+table["vars"]}

class Select(Base):
	""" 
		Builds select query 
	"""
	# SELECT field FROM table
	# WHERE cond AND cond OR (cond AND cond)
	# LEFT JOIN table ON cond

	# fields is string keys
	def __init__(self, *fields, table):
		self._main_table = table
		self._tables = [table]
		self._fields = [] 
		self.add_fields(*fields, table=table)

		self._page_size = None
		self._page_num = None

		self._joins = []
	
	def add_fields(self, *fields, table):
		if len(fields) == 0:
			any(self._fields.append(F.E_FIELD(table, f)) for f, v in table.get_fields().items())
		elif len(fields) >= 1 and fields[0] == None:
			pass
		else:
			any(self._fields.append(F.E_FIELD(table, f)) for f in fields if (not f is None))

		for i, f in enumerate(self._fields):
			f.field.alias =  self.get_field_alias(i)

	# SQL statements #

	def where(self, cond):
		self._where = cond

	def join(self, *fields, table, mod=None, cond=None): # dont use cond plss. dont really know for what this
		"""
			Available left, right, full outer and inner
		"""
		join_cond = None
		for t in self._tables:
			join_cond = model.get_dependencies(table, t) or model.get_dependencies(t, table)
			if not join_cond is None:
				break			
		
		if cond is None and join_cond is None:
			return -1

		join_cond = M_COMP(join_cond[0], "=", join_cond[1])
		if not cond is None:
			join_cond = M_COND(join_cond, cond, "AND")

		self._joins.append(Join(table, mod, join_cond))

		self._tables.append(table)
		
		self.add_fields(*fields, table=table)

	def order_by(self, *nums):
		if not hasattr(self, "_order_by"):
			self._order_by = []
		self._order_by += list(nums)
	# ============== #

	def get_tables(self):
		return self._tables

	def get_fields(self):
		return self._fields

	def get_field_alias(self, id):
		return "id" + str(id)

	def get_fields_caption(self):
		return tuple(f.field.caption for f in self._fields)

	def get_count(self, execute=None):
		f = self.finish(no_pagenation=True)
		query = "SELECT COUNT(*) FROM ({})".format(f["query"])
		if execute is None:
			return {"query": query, "vars": f["vars"]}
		else:
			return execute({ "query": query, "vars": f["vars"]})[0][0]

	@property
	def page_size(self):
		return self._page_size

	@page_size.setter
	def page_size(self, value):
		self._page_size = value

	@property
	def page_num(self):
		return self.page_num

	@page_num.setter
	def page_num(self, value):
		self._page_num = value

	def copy(self):
		sel = copy.deepcopy(self)
		return sel

	def finish(self, **keys):
		# page from 0 to n
		vars = []
		query = "SELECT"
		
		if self._page_size != None and not("no_pagenation" in keys):
			query += " FIRST {} SKIP {}".format(self._page_size, (self._page_num or 0) * self._page_size)	

		if len(self._fields) == 0:
			query += " *"
		else:
			for i, f in enumerate(self._fields):
				query += " {}.{} AS {},".format(f.table.name, f.field.name, f.field.alias)
			query = query[:-1]
		
		query, vars = add_statement_with_vars(query, vars, " FROM {}", self._main_table.get_sql())

		for j in self._joins:
			query, vars = add_statement_with_vars(query, vars, " {}", j())
		
		if hasattr(self, "_where"):
			query, vars = add_statement_with_vars(query, vars, " \nWHERE {}", self._where())

		if hasattr(self, "_order_by"):
			query += " \nORDER BY"
			for n in self._order_by:
				query += " {},".format(n)
			query = query[:-1]

		return {"query": query, "vars": vars}

	def create_table(self, caption="SELECT таблица"):
		table = model.SelectTable( sel )
		return table

class Update(Base):

	def __init__(self, table, id):
		self._table = table
		self._id = id
		self._columns = []

	def set(self, col, value):
		# col is key or id
		self._columns.append({"query": "{} = ?".format(self._table[col].name), "vars": [value]})

	def finish(self):
		query, vars = add_statement_with_vars("", [], "UPDATE {}\n", self._table.get_sql())
		
		if len(self._columns) > 0:
			query += "SET"
			for c in self._columns:
				query, vars = add_statement_with_vars(query, vars, " {},", c)
			query = query[:-1]
			query += "\n"
		else:
			return {"query": "", "vars": []}

		query, vars = add_statement_with_vars(query, vars, "{}", {"query": "WHERE id = ?", "vars": [self._id]})

		return {"query": query, "vars": vars}

class Delete(Base):

	def __init__(self, table, id):
		self._table = table
		self._id = id

	def finish(self):
		query, vars = add_statement_with_vars("", [], "DELETE FROM {}\n", self._table.get_sql())
		query, vars = add_statement_with_vars(query, vars, "{}", {"query": "WHERE id = ?", "vars": [self._id]})
		return {"query": query, "vars": vars}

class Insert(Base):

	def __init__(self, table, values):
		self._table = table
		self._values = values

	def finish(self):
		query, vars = add_statement_with_vars("", [], "INSERT INTO {} (", self._table.get_sql())
		for k, f in self._table.get_fields().items():
			if k == "id":
				continue
			query += f.name + ","
		query = query[:-1] + ") \n"
		query += ""
		query += "VALUES ("
		for v in self._values:
			query, vars = add_statement_with_vars(query, vars, "{},", {"query": "?", "vars": [v]})
		query = query[:-1] + ")"

		return {"query": query, "vars": vars}
