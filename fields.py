# - *- coding: utf- 8 - *-

#############
# fields.py #
#############

import copy

import model as M
import sqlbuilder as SQL

class ExtendedField:
	def __init__(self, table, key):
		self._table = table
		self._key = key

	@property
	def table(self):
		return self._table

	@property
	def key(self):
		return self._key

	@property
	def field(self):
		return self._table[self._key]

	def get_values(self):
		target = self.field.e_target or self
		sel = SQL.Select(target.key, table=target.table)
		values = SQL.execute(sel)
		return [v[0] for v in values]

E_FIELD = ExtendedField

primitives = (int, str)

def is_primitive(v):
	return type(v) in primitives

class Base:
	def __init__(self, name, caption=None):
		self._name = name
		self._caption = caption if caption else name.upper()
		self._alias = None

	@property
	def name(self):
		return self._name

	@name.setter
	def name(self, value):
		self._name = value

	@property
	def caption(self):
		return self._caption

	@property
	def alias(self):
		return self._alias

	@alias.setter
	def alias(self, value):
		self._alias = value

	@caption.setter
	def caption(self, value):
		self._caption = value

	def __repr__(self):
		return "([{} {}] : '{}' on {})".format(self.__class__.__name__, self._name, self._caption, hex(id(self)))

	def comparable(self, a):
		if isinstance(a, E_FIELD):
			return issubclass(a.field.__class__, self.__class__)
		elif issubclass(a.__class__, Base.__class__):
			return isinstance(a, self.__class__)
		elif type(a) in primitives: # is values like str or int
			a = self.cast(a)
			return not a is None
		return None

	@property
	def target(self):
		return self

	@property
	def e_target(self):
		return None

	def copy(self):
		f = copy.deepcopy(self)
		return f

class Integer(Base):

	@classmethod
	def type_name():
		return "целое число (int)"

	def short(self):
		return "int"

	def cast(self, a):
		try:
			a = int(a)
		except Exception:
			a = None
		return a 

class ForeignKey(Integer):
	def __init__(self, name, caption, table, id_field, target_field):
		super().__init__(name, caption)
		self._table = table
		self._field = id_field
		self._target_field = target_field

	@property
	def table(self):
		return self._table

	@property
	def field(self):
		return self._field

	def short(self):
		return "foreign"

	@Base.target.getter
	def target(self):
		table = M.get_model().get_table_by_class(self._table)
		return table[self._target_field]

	@Base.e_target.getter
	def e_target(self):
		table = M.get_model().get_table_by_class(self._table)
		return E_FIELD(table, self._target_field)

class String(Base):

	@classmethod
	def type_name():
		return "строка (str)"

	def cast(self, a):
		return str(a)

	def short(self):
		return "str"
