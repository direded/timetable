# - *- coding: utf- 8 - *-

############
# model.py #
############

from flask import g

import fields as F
import sqlbuilder as SQL

import collections as COL


def get_model():
	if not hasattr(g, "model"):
		g.model = Model()
		g.model.build_pretty_model()
	return g.model

def get_dependencies(ref_table, table):
	for k, v in sorted(ref_table.get_fields().items()):
		if isinstance(v, F.ForeignKey):
			if v.table == table.__class__:
				return(F.E_FIELD(ref_table, k), F.E_FIELD(table, v.field))
	return None

class ModelTable:
	pass

class BaseTable:
	def __init__(self):
		self._caption = "Table"
		self._fields = COL.OrderedDict()

	@property
	def name(self):
		a = self.__class__.__name__
		return a[2:].upper()

	@property
	def caption(self):
		return self._caption

	@caption.setter
	def caption(self, a):
		self._caption = a

	def get_sql(self):
		return {
				"query": self.name,
				"vars": [],
			}

	def get_fields(self):
		return self._fields

	def __getitem__(self, key):
		if isinstance(key, int):
			i = 0
			for k, v in self.get_fields().items():
				if i == key:
					return v
				i += 1
		else:
			return self.get_fields()[key]
		raise KeyError


	def build_pretty_table(self):
		pass

	@property
	def pretty(self):
		return self._pretty if hasattr(self, "_pretty") else self

	@property
	def original(self):
		return self._original if hasattr(self, "_original") else self

	def get_fields_name(self):
		return tuple(f.name for k, f in self.get_fields().items())

	def get_fields_caption(self):
		return tuple(f.caption for k, f in self.get_fields().items())

class DependentTable(BaseTable):

	def build_pretty_table(self):
		sql = self.build_pretty_select()
		self._pretty = SelectTable(sql)
		self._pretty._original = self
		self._pretty._caption = self._caption

	def build_pretty_select(self):
		pass

class SelectTable(BaseTable):
	def __init__(self, select):
		super().__init__()

		self._select = select.copy()
		self.rebuild()
		
	@BaseTable.name.getter
	def name(self):
		return "\"{}\"".format(self.get_alias())

	def rebuild(self):
		s = self._select
		self._fields = { f.field.alias: f.field.copy() for i, f in enumerate(s.get_fields()) }
		for k, f in sorted(self._fields.items()):
			if f.alias is None:
				f.alias = k
			else:
				f.name = f.alias

	def get_alias(self):
		return str(id(self))

	def get_sql(self):
		f = self._select.finish()
		query = "({}) AS {}".format(f["query"], self.name)
		vars = f["vars"]
		return {"query": query, "vars": vars}

class T_Weekdays(ModelTable, BaseTable):

	def __init__(self):	
		super().__init__()

		self._caption = "Дни недели" 
		self._fields["id"] = F.Integer("id")
		self._fields["name"] = F.String("name", "День недели")
		self._fields["order"] = F.Integer("order_number", "Порядок")
	
class T_Lessons(ModelTable, BaseTable):
	def __init__(self):	
		super().__init__()

		self._caption = "Порядок занятий"
		self._fields["id"] = F.Integer("id")
		self._fields["name"] = F.String("name", "Номер пары")
		self._fields["order"] = F.Integer("order_number", "Порядок")

class T_Subjects(ModelTable, BaseTable):
	def __init__(self):	
		super().__init__()

		self._caption = "Предметы"
		self._fields["id"] = F.Integer("id")
		self._fields["name"] = F.String("name", "Предмет")

class T_Audiences(ModelTable, BaseTable):
	def __init__(self):	
		super().__init__()

		self._caption = "Аудитории"
		self._fields["id"] = F.Integer("id")
		self._fields["name"] = F.String("name", "Номер аудитории")

class T_Groups(ModelTable, BaseTable):
	def __init__(self):	
		super().__init__()

		self._caption = "Группы"
		self._fields["id"] = F.Integer("id")
		self._fields["name"] = F.String("name", "Номер группы")

class T_Teachers(ModelTable, BaseTable):
	def __init__(self):	
		super().__init__()

		self._caption = "Преподаватели"
		self._fields["id"] = F.Integer("id")
		self._fields["name"] = F.String("name", "Имена преподавателей")

class T_Lesson_types(ModelTable, BaseTable):
	def __init__(self):	
		super().__init__()

		self._caption = "Типы занятий"
		self._fields["id"] = F.Integer("id")
		self._fields["name"] = F.String("name", "Тип")

class T_Subject_group(ModelTable, DependentTable):
	def __init__(self):	
		super().__init__()

		self._caption = "Предмет-группа"
		self._fields["id"] = F.Integer("id")
		self._fields["subject_id"] = F.ForeignKey("subject_id", "ID предмета", T_Subjects, "id", "name")
		self._fields["group_id"] = F.ForeignKey("group_id", "ID группы", T_Groups, "id", "name")

	def build_pretty_select(self):
		model = get_model()
		sql = SQL.Select("id", table=self)
		sql.join("name", mod="left", table=model.get_table_by_class(T_Subjects))
		sql.join("name", mod="left", table=model.get_table_by_class(T_Groups))
		
		return sql

class T_Subject_teacher(ModelTable, DependentTable):
	def __init__(self):	
		super().__init__()

		self._caption = "Предмет-преподаватель"
		self._fields["id"] = F.Integer("id")
		self._fields["subject_id"] = F.ForeignKey("subject_id", "ID предмета", T_Subjects, "id", "name")
		self._fields["teacher_id"] = F.ForeignKey("teacher_id", "ID преподаватель", T_Teachers, "id", "name")

	def build_pretty_select(self):
		model = get_model()
		sql = SQL.Select("id", table=self)
		sql.join("name", mod="left", table=model.get_table_by_class(T_Subjects))
		sql.join("name", mod="left", table=model.get_table_by_class(T_Teachers))

		return sql

class T_Sched_items(ModelTable, DependentTable):
	def __init__(self):	
		super().__init__()

		self._caption = "Расписание"
		self._fields["id"] = F.Integer("id")
		self._fields["lesson_id"] = F.ForeignKey("lesson_id", "ID порядка", T_Lessons, "id", "name")
		self._fields["subject_id"] = F.ForeignKey("subject_id", "ID предмета", T_Subjects, "id", "name")
		self._fields["audience_id"] = F.ForeignKey("audience_id", "ID аудитории", T_Audiences, "id", "name")
		self._fields["group_id"] = F.ForeignKey("group_id", "ID группы", T_Groups, "id", "name")
		self._fields["teacher_id"] = F.ForeignKey("teacher_id", "ID преподавателя", T_Teachers, "id", "name")
		self._fields["type_id"] = F.ForeignKey("type_id", "ID типа", T_Lesson_types, "id", "name")
		self._fields["weekday_id"] = F.ForeignKey("weekday_id", "ID дня", T_Weekdays, "id", "name")

	def build_pretty_select(self):
		model = get_model()
		sql = SQL.Select("id", table=self)
		sql.join("name", mod="left", table=model.get_table_by_class(T_Lessons))
		sql.join("name", mod="left", table=model.get_table_by_class(T_Subjects))
		sql.join("name", mod="left", table=model.get_table_by_class(T_Audiences))
		sql.join("name", mod="left", table=model.get_table_by_class(T_Groups))
		sql.join("name", mod="left", table=model.get_table_by_class(T_Teachers))
		sql.join("name", mod="left", table=model.get_table_by_class(T_Lesson_types))
		sql.join("name", mod="left", table=model.get_table_by_class(T_Weekdays))

		return sql

####################
####################
####################

class TableList:

	def __init__(self, tables=None):
		self._tables = tables or []

	def get_tables(self):
		return self._tables

	def get_tables_caption(self):
		return tuple(t.caption for t in self._tables)

	def get_tables_name(self):
		return tuple(t.name for t in self._tables)

	def get_table(self, num):
		return self._tables[num]

	def get_table_by_name(self, name):
		for t in self._tables:
			if t.name == name:
				return t
		return None

	def get_table_by_class(self, name):
		for t in self._tables:
			if t.__class__ == name:
				return t
		return None

	def get_table_by_class_name(self, name):
		for t in self._tables:
			if t.__class__.__name__.upper() == name.upper():
				return t
		return None

	def __getitem__(self, key):
		if isinstance(key, str):
			return self.get_table_by_name(str)
		return self.get_table(key)

	def add(self, table):
		self._tables.append(table)


class Model(TableList):

	def __init__(self):
		super().__init__()
		self._tables = [c() for c in ModelTable.__subclasses__()]

	def build_pretty_model(self):
		any(t.build_pretty_table() for t in self._tables)

	@property
	def pretty(self):
		return TableList([t.pretty for t in self._tables])